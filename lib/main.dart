// import 'dart:html';

import 'package:flutter/material.dart';
import 'package:payment_app/theme.dart';

void main() {
  runApp(PaymentApps());
}

class PaymentApps extends StatefulWidget {
  @override
  _PaymentAppsState createState() => _PaymentAppsState();
}

class _PaymentAppsState extends State<PaymentApps> {
  int selectedIndex = -1;
  @override
  Widget build(BuildContext context) {
    Widget checkoutButton() {
      return Column(
        children: [
          Container(
            width: 311,
            height: 52,
            child: TextButton(
              style: TextButton.styleFrom(
                  backgroundColor: Color(0xFF007DFF),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(71),
                  )),
              onPressed: () {},
              child: Text(
                "Checkout Now",
                style: btnTitleTextStyle,
              ),
            ),
          ),
        ],
      );
    }

    Widget option(int index, String title, String desc, String price) {
      return GestureDetector(
        onTap: () {
          setState(() {
            selectedIndex = index;
          });
        },
        child: Container(
          padding: EdgeInsets.all(20),
          margin: EdgeInsets.all(10),
          decoration: BoxDecoration(
            border: Border.all(
              color: selectedIndex == index
                  ? Color(0xFF007DFF)
                  : Color(0xFF4D5B7C),
            ),
            borderRadius: BorderRadius.circular(14),
          ),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  selectedIndex == index
                      ? Image.asset(
                          'assets/check_two.png',
                          width: 25,
                          height: 25,
                        )
                      : Image.asset(
                          'assets/check.png',
                          width: 25,
                          height: 25,
                        ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        title,
                        style: planTitleTextStyle,
                      ),
                      Text(
                        desc,
                        style: descTitleTextStyle,
                      ),
                    ],
                  ),
                  SizedBox(
                    width: 91,
                  ),
                  Row(
                    children: [
                      Text(
                        price,
                        style: priceTitleTextStyle,
                      )
                    ],
                  ),
                ],
              )
            ],
          ),
        ),
      );
    }

    Widget header() {
      return Padding(
        padding: const EdgeInsets.only(top: 50, left: 32, right: 32),
        child: Column(
          children: [
            Center(
              child: Image.asset(
                'assets/icon_one.png',
                width: 267,
                height: 200,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 50),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'Upgrade to',
                    style: titleTextStyle,
                  ),
                  SizedBox(
                    width: 6,
                  ),
                  Text(
                    'Pro',
                    style: secondaryTextStyle,
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 16),
              child: Text(
                'Go Unlock all features and \nbuild your own business bigger',
                style: subTitleTextStyle,
                textAlign: TextAlign.center,
              ),
            )
          ],
        ),
      );
    }

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: Color(0xFF04112F),
        body: SingleChildScrollView(
          child: Column(
            children: [
              header(),
              SizedBox(
                height: 50,
              ),
              option(0, "Monthly", "Good for starting Up", "\$20"),
              option(1, "Quarterly", "Focusing on building", "\$220"),
              option(2, "Yearly", "Steady Company", "\$50"),
              option(3, "Hours", "Startup Company", "\$150"),
              SizedBox(
                height: 20,
              ),
              selectedIndex == -1 ? SizedBox() : checkoutButton(),
              SizedBox(
                height: 20,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
