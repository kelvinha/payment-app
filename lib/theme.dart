import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

Color whiteColor = Color(0xffFFFFFF);

TextStyle titleTextStyle = GoogleFonts.poppins(
  color: whiteColor,
  fontWeight: FontWeight.bold,
  fontSize: 26,
);

TextStyle secondaryTextStyle = GoogleFonts.poppins(
  color: Color(0xff007DFF),
  fontWeight: FontWeight.bold,
  fontSize: 26,
);

TextStyle subTitleTextStyle = GoogleFonts.poppins(
  color: Color(0xff899788),
  fontWeight: FontWeight.w300,
  fontSize: 16,
);

TextStyle planTitleTextStyle = GoogleFonts.poppins(
  color: whiteColor,
  fontWeight: FontWeight.w500,
  fontSize: 14,
);

TextStyle descTitleTextStyle = GoogleFonts.poppins(
  color: whiteColor,
  fontWeight: FontWeight.w300,
  fontSize: 12,
);

TextStyle priceTitleTextStyle = GoogleFonts.poppins(
  color: whiteColor,
  fontWeight: FontWeight.bold,
  fontSize: 18,
);

TextStyle btnTitleTextStyle = GoogleFonts.poppins(
  color: whiteColor,
  fontWeight: FontWeight.w500,
  fontSize: 16,
);
